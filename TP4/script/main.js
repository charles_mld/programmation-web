let theTableElement;
let httpRequest;
let intervalId;
let cpt = 0;
const apiURL = "https://randomuser.me/api/";

window.addEventListener("DOMContentLoaded",init);

function init(){ /on aurait aussi pu faire une fonction anonyme/
	theTableElement = document.getElementById('theTable');
	httpRequest = new XMLHttpRequest(); /on créé l'objet http request/

    httpRequest.onreadystatechange = addLineToTheTable; 

    intervalId = setInterval(
    	function(){ /anonyme car on l'utilise que dans init/
    		if(cpt==10){ /pour ne faire que 10 appels de personnes/
    			clearInterval(intervalId);
    		}else{
    			httpRequest.open('GET', apiURL); /demande à httprequest de faire une demande http à l'api (il la prpeare)/
    			httpRequest.send(); /on envoie cette requete/
    			cpt++;
    		}
    	},
    	500);
}

function addLineToTheTable(){
	if (httpRequest.readyState === XMLHttpRequest.DONE) { /comparaison sur la valeur ET le type/
      if (httpRequest.status === 200) {
      	const response = JSON.parse(httpRequest.responseText); /méthode parse de type JSONresponse est un tableau/
      	const prénom = response.results[0].name.first;
      	const nom = response.results[0].name.last;
      	const newTableRow = document.createElement('tr'); /élément ligne (table row)/
      	const newTableDivFirst = document.createElement('td'); /élément division (table division)/
      	newTableDivFirst.innerText = prénom; 
      	newTableRow.appendChild(newTableDivFirst);
      	const newTableDivLast = document.createElement('td');
      	newTableDivLast.innerText = nom;
      	newTableRow.appendChild(newTableDivLast);
      	theTableElement.appendChild(newTableRow);
      } else {
        console.log('Il y a eu un problème avec la requête. Le code de retour est '+ httpRequest.status);
      }
    }
}