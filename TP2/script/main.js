/*
	Les commentaires multi-ligne JS s'écrivent ainsi.
	Cela permet de mettre un texte assez long et
		formaté. 
*/

// les commentaires en ligne s'écrivent ainsi.


/*
	Ce fichier est interprété par l'interpréteur JS du navigateur 
	lorsque l'interpréteur HTML lui passe la main, c'est à dire 
	à la lecture de la balise script dans le fichier HTML.	
*/ 

// Première instruction exécutée.
/* 
	On demande à JS d'observer l'élément "window" (l'élément le plus global du DOM).
	Si jamais l'événement "DOMContentLoaded" survient sur cet élément, 
	alors il faudra que JS exécute la fonction nommée "init" (déclarée après). 
	La fonction "init" est donc une fonction de _callback_ puisqu'elle est appelée en retour d'un événement...   

	NB1: la méthode addEventListner est présente dans tous les éléments du DOM (boutons, images, div, ... etc).
	NB2: les événements qui peuvent survenir sur un élément dépendent de cet élément.
	NB3: on peut créer ses propres événements.
*/
window.addEventListener("DOMContentLoaded",init);

// déclaration de la fonction init
function init(){
	/* 
		on accède au DOM en manipulant l'objet JS "document". 
		Cet objet existe dans l'espace d'exécution.
		Il est initialisé par le moteur JS.
	*/ 
	// ici, on crée un nouvel élément de nature div. Il n'est pas encore inséré dans la structure DOM.
	const aNewDivElement = document.createElement('div');

	// on identifie cette nouvelle div
	aNewDivElement.setAttribute('id','childDiv');
	
	// on récupère un élément du DOM à partir de son id.
	// NB : il est possible de récupérer un ou plusieurs élements à partir de leur nature (h1, span, ...), de leur classe ou de leur position (fils de ...). 
	const theParentDivElement=document.getElementById('parentDiv');
	
	// on attache la div "aNewDivElement" au DOM : cet élement devient le fils de l'élément "theParentDiv" 
	theParentDivElement.appendChild(aNewDivElement);
}